# Laravel Project with Nuxt.js Frontend

This project is a web application using Laravel as the backend and Nuxt.js for the frontend.

## Prerequisites

- PHP 7.4 or higher
- Composer
- Node.js and npm
- A database server (MySQL, PostgreSQL, etc.)

## Installation

### Laravel Backend

1. **Clone the repository:**
   ```bash
   git clone https://gitlab.com/noyer.coralie/stars-laravel-nuxt.git (or with SSH)
   cd your-project-directory

2. **Install PHP dependecies:**
   ```bash
   composer install
   php artisan breeze:install api
   
3. **Create the .env file and configure your environment:**
   ```bash
   cp .env.example .env
   
4. **Generate the application key:**
   ```bash
   php artisan key:generate

4. **Create the database (example for sqlite):**
   Create a database.sqlite file in /database
   
5. **Link the public storage directory:**
To make files in the storage/app/public directory accessible from the web, create a symbolic link:
      ```bash
      php artisan storage:link
This command links the public storage directory to the public/storage directory in your application, making it accessible via the web.
   
### Nuxt.js FrontEnd

1. **Navigate to frontend:**
   ```bash
   cd frontend

2. **Install Node.js dependecies:**
   ```bash
   npm install
   
### Database configuration

1. **Run migrations to create the database tables:**
   ```bash
   php artisan migrate

2. **Seed the database with initial data using seeders:**
   ```bash
   php artisan db:seed

### Running projects

1. **Run laravel:**
   ```bash
   php artisan serve

2. **Run nuxtjs:**
   ```bash
   cd /frontend
   npm run dev
   
3. **Run laravel test:**
   ```bash
   php artisan test

### Improvement suggestions
- Prettier
- GitLab CI
- Translation
- Front-end testing
- Larastan
- Pre-commit hook
- Ability to register
- Improve the image service
- Text editor

I used SQLite to facilitate development, but we could switch to MySQL.
