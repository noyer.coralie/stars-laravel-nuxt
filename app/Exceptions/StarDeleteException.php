<?php
namespace App\Exceptions;

use Exception;

class StarDeleteException extends Exception
{
    public function __construct($message = 'Error deleting star', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
