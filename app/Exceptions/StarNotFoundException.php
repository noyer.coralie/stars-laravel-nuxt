<?php
namespace App\Exceptions;

use Exception;

class StarNotFoundException extends Exception
{
    public function __construct($message = 'Star not found', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
