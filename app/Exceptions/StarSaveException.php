<?php
namespace App\Exceptions;

use Exception;

class StarSaveException extends Exception
{
    public function __construct($message = 'Error saving star', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
