<?php

namespace App\Http\Controllers;

use App\Exceptions\StarNotFoundException;
use App\Exceptions\StarSaveException;
use App\Http\Requests\StarStoreUpdateRequest;
use App\Models\Star;
use App\Services\ImageService;
use App\Services\StarService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class StarController extends Controller
{

    protected $starService;
    protected $imageService;

    public function __construct(StarService $starService, ImageService $imageService)
    {
        $this->starService = $starService;
        $this->imageService = $imageService;
    }
    /**
     * List all stars.
     *
     * @return Response
     */
    public function list()
    {
        try {
            $stars = $this->starService->list();
            return response()->json(['success' => true, 'data' => $stars]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get a star by ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function getById($id)
    {
        try {
            $star = $this->starService->getById($id);
            return response()->json(['success' => true, 'data' => $star]);
        } catch (StarNotFoundException $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }
    }

    /**
     * Create or update a star.
     *
     * @param  StarStoreUpdateRequest  $request
     * @param  int|null  $id
     * @return Response
     */
    public function save(StarStoreUpdateRequest $request, $id = null)
    {
        try {
            $data = $request->only('name', 'firstName', 'description');

            if ($request->hasFile('image')) {
                $data['image'] = $this->imageService->store($request->file('image'));
            } elseif ($request->input('image')) {
                $data['image'] = $request->input('image');
            }

            $star = $id ? $this->starService->save($data, $id) : $this->starService->save($data);

            return response()->json(['success' => true, 'data' => $star]);
        } catch (StarSaveException $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Delete a star.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        try {
            $this->starService->delete($id);
            return response()->json(['success' => true, 'message' => 'Star deleted successfully']);
        } catch (StarNotFoundException $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 404);
        } catch (StarDeleteException $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }
}
