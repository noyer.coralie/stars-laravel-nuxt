<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Star extends Model
{
    use HasFactory;
    protected $appends = ['imageUrl'];
    protected $fillable = ['name', 'firstName', 'image', 'description'];

    /**
     * Get the image URL attribute.
     *
     * @return string|null
     */
    public function getImageUrlAttribute()
    {
        if ($this->image && !filter_var($this->image, FILTER_VALIDATE_URL)) {
            // build image url from storage
            return Storage::url($this->image);
        }

        // return image url
        return $this->image;
    }
}
