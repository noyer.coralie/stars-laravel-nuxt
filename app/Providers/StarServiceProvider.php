<?php

namespace App\Providers;

use App\Services\StarService;
use Illuminate\Support\ServiceProvider;

class StarServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(StarService::class, function ($app) {
            return new StarService();
        });
    }
}
