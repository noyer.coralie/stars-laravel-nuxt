<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ImageOrUrl implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        // Check if value is valid url
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return ;
        }

        if ($value instanceof UploadedFile) {
            // If it's a file, check if it's a valid image
            $validator = Validator::make([$attribute => $value], [
                $attribute => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            if ($validator->fails()) {
                $fail('The ' . $attribute . ' must be a valid image file.');
                return;
            }

            return;
        }

        // If neither a URL nor a file image is provided, fail the validation.
        $fail('The ' . $attribute . ' must be a valid URL or an image file.');
    }
}
