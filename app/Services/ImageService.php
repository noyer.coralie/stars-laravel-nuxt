<?php
namespace App\Services;



use Illuminate\Http\UploadedFile;

class ImageService
{
    public function store(UploadedFile $file)
    {
        // unique filename
        $fileName = uniqid() . '.' . $file->getClientOriginalExtension();

        // save image
        $filePath = $file->storeAs('images', $fileName, 'public');

        // return complete path
        return $filePath;
    }
}
