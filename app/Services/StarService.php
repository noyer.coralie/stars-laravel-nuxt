<?php
namespace App\Services;

use App\Exceptions\StarDeleteException;
use App\Exceptions\StarNotFoundException;
use App\Exceptions\StarSaveException;
use App\Models\Star;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StarService
{
    public function list()
    {
        return Star::all();
    }

    public function getById($id)
    {
        try {
            return Star::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new StarNotFoundException("Star with ID $id not found");
        }
    }

    public function save($data, $id = null)
    {
        try {
            if ($id) {
                $star = Star::findOrFail($id);
                $star->update($data);
            } else {
                $star = Star::create($data);
            }
            return $star;
        } catch (\Exception $e) {
            throw new StarSaveException("Error saving star: " . $e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $star = Star::findOrFail($id);
            $star->delete();
            return true;
        } catch (ModelNotFoundException $e) {
            throw new StarNotFoundException("Star with ID $id not found");
        } catch (\Exception $e) {
            throw new StarDeleteException("Error deleting star: " . $e->getMessage());
        }
    }
}
