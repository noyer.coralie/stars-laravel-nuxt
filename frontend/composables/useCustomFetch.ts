// import type { UseFetchOptions } from '#app'
//
// export function useCustomFetch<T> (path: string | (() => string), options: UseFetchOptions<T> = {}) {
//     let headers: Record<string, string> = {
//
//     }
//     const token = useCookie('XSRF-TOKEN')
//
//
//
//     if(token.value){
//         headers['X-XSRF-TOKEN'] = token.value as string
//     }
//
//     if(process.server){
//         //get cookie and referer
//         headers = {
//             ...headers,
//             ...useRequestHeaders(["referer", "cookie"])
//         }
//     }
//
//     return useFetch(`http://localhost:8000${path}`, {
//         credentials: "include",
//         //watch: false => prevent multiple submit
//         watch: false,
//         ...options,
//         headers: {
//             ...headers,
//             ...options?.headers
//         }
//     })
// }
import type { UseFetchOptions } from '#app'

export async function useCustomFetch<T>(path: string | (() => string), options: UseFetchOptions<T> = {}) {
    let headers: Record<string, string> = {}
    const token = useCookie('XSRF-TOKEN')


    if (token.value) {
        headers['X-XSRF-TOKEN'] = token.value as string
    }

    if (process.server) {
        //get cookie and referer
        headers = {
            ...headers,
            ...useRequestHeaders(["referer", "cookie"])
        }
    }

    const fetchResult = await useFetch(`http://localhost:8000${path}`, {
        credentials: "include",
        //watch: false => prevent multiple submit
        watch: false,
        ...options,
        headers: {
            ...headers,
            ...options?.headers
        }
    })

    if (process.client) {
        await fetchResult.pending;
    }

    if (fetchResult.error.value?.data?.message) {
        const error = new Error(fetchResult.error.value?.data?.message || 'An error occurred during the API call');
        throw error;
    }

    return fetchResult;

}
