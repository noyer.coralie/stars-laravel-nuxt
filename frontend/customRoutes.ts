import { resolve } from 'path'
export const customRoutes = [
    {
        name: 'starId',
        path: '/admin/star/:id?',
        file: resolve(__dirname, 'pages/admin/star.vue')
    },
];
