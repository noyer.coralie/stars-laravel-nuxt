import {useAuthStore} from "../stores/useAuthStore";

export default defineNuxtRouteMiddleware(async() => {
    const auth = useAuthStore();

    await auth.fetchUser()


    if(auth.isLoggedIn){
       return navigateTo('/admin', {replace: true})
    }

})
