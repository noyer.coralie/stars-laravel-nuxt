// https://nuxt.com/docs/api/configuration/nuxt-config
import {customRoutes} from "./customRoutes";

export default defineNuxtConfig({
  devtools: { enabled: true },
    typescript: {
        typeCheck: true
    },
    modules: [
        '@nuxtjs/tailwindcss',
        '@nuxtjs/eslint-module',
        '@pinia/nuxt',
    ],
    // Enables having the same page for creating and editing a star, with an id as a parameter.
    hooks: {
        "pages:extend": (pages) => {
            customRoutes.forEach((customRoute) => {
                pages.push(customRoute)
            })
        }
    }
})
