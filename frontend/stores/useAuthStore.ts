import {defineStore} from "pinia";

type User = {
    id: number;
    name: string;
    email: string;
}

type Credentials = {
    email: string;
    password: string;
}

export const useAuthStore = defineStore('auth', () => {
    const user = ref<User | null>(null);
    const isLoggedIn = computed(() => !!user.value)

    async function logout(){
        await useCustomFetch("/logout", {method: "POST"})
        user.value = null
        navigateTo('/login')
    }

    async function fetchUser(){
        const response = await useCustomFetch("/api/user")
        if (response.data.value) {
            user.value = response.data.value as User;
        }
    }

    async function login(credentials: Credentials) {

        await useCustomFetch("/sanctum/csrf-cookie")

        const response = await useCustomFetch("/login", {
            method: "POST",
            body: credentials,
        })

        if (response.data) {
            await fetchUser();
        }

        return response;

    }
    return {user, login, isLoggedIn, fetchUser, logout}
})
