import {defineStore} from "pinia";


export const useErrorStore = defineStore('error', () => {
    const error = ref<string | null>(null);
    const showError = ref<boolean>(false)

    function setError(message: string){
        error.value = message;
        showError.value = true
    }

    function clearError(){
        error.value = null;
        showError.value = false;
    }

    return {error, showError, setError, clearError}
})
