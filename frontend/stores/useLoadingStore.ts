import {defineStore} from "pinia";


export const useLoadingStore = defineStore('loading', () => {
    const isLoading = ref<boolean>(false)

    function showLoader(){
        isLoading.value = true
    }

    function hideLoader(){
        isLoading.value = false
    }

    return {isLoading, showLoader, hideLoader}
})
