import {Ref} from "vue";

export interface Star {
    id?: number,
    name: string,
    firstName: string,
    image: string|null,
    imageUrl: string|null,
    description: string|null
}

export interface ApiResponse<T> {
    data: Ref<T|undefined>,
    success: boolean
}
