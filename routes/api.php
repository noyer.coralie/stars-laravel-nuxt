<?php

use App\Http\Controllers\StarController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

//routes for authenticated users
Route::middleware('auth:sanctum')->group(function () {
    Route::post('/stars', [StarController::class, 'save']); //create
    Route::post('/stars/{id}', [StarController::class, 'save']); //update
    Route::delete('/stars/{id}', [StarController::class, 'delete']);
});

//public routes
Route::get('/stars', [StarController::class, 'list']);
Route::get('/stars/{id}', [StarController::class, 'getById']);
