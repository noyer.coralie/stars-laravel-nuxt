<?php

namespace Tests\Unit\Models;

use App\Models\Star;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class StarTest extends TestCase
{
    use RefreshDatabase;

    public function testReturnsAUrlForTheImageIfTheImageIsAPath()
    {
        Storage::fake('public');
        $file = Storage::disk('public')->put('images', 'testImage.jpg');

        $star = Star::create([
            'name' => 'Test Star',
            'firstName' => 'Test',
            'image' => $file,
            'description' => 'A star'
        ]);

        $this->assertStringContainsString('/storage/' . $file, $star->imageUrl);
    }

    public function testReturnsTheImageFieldDirectlyIfItIsAlreadyAUrl()
    {
        $star = Star::create([
            'name' => 'Test Star',
            'firstName' => 'Test',
            'image' => 'http://example.com/image.jpg',
            'description' => 'A star'
        ]);

        $this->assertEquals('http://example.com/image.jpg', $star->imageUrl);
    }
}
