<?php

namespace Tests\Feature;

use App\Models\Star;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class StarControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testListStarsUnauthenticated()
    {
        $response = $this->getJson('/api/stars');

        $response->assertStatus(200); // Should be accessible without authentication.
    }

    public function testListStarsAuthenticated()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->getJson('/api/stars');

        $response->assertStatus(200); // Should be accessible while authenticated.
    }

    public function testGetStarByIdUnauthenticated()
    {
        $star = Star::factory()->create();

        $response = $this->get("/api/stars/{$star->id}");

        $response->assertStatus(200)
            ->assertJson([
                'success' => true,
                'data' => [
                    'name' => $star->name
                ]
            ]);
    }

    public function testGetStarByIdAuthenticated()
    {
        $user = User::factory()->create();
        $star = Star::factory()->create();

        $response = $this->actingAs($user)->get("/api/stars/{$star->id}");

        $response->assertStatus(200)
            ->assertJson([
                'success' => true,
                'data' => [
                    'name' => $star->name
                ]
            ]);
    }

    public function testCreateStarUnauthenticated()
    {
        $starData = Star::factory()->make()->toArray();

        $response = $this->postJson('/api/stars', $starData);

        $response->assertStatus(401); // Unauthorized
    }

    public function testUpdateStarUnauthenticated()
    {
        $star = Star::factory()->create();
        $updatedData = ['name' => 'Updated Name', 'firstName' => 'Updated first name'];

        $response = $this->postJson("/api/stars/{$star->id}", $updatedData);

        $response->assertStatus(401); // Unauthorized
    }

    public function testDeleteStarUnauthenticated()
    {
        $star = Star::factory()->create();

        $response = $this->deleteJson("/api/stars/{$star->id}");

        $response->assertStatus(401); // Unauthorized
    }

    public function testCreateStarAuthenticated()
    {
        $user = User::factory()->create();
        $starData = Star::factory()->make()->toArray();

        $response = $this->actingAs($user)->postJson('/api/stars', $starData);

        $response->assertStatus(200);
        $this->assertDatabaseHas('stars', [
            'name' => $starData['name'],
            'firstName' => $starData['firstName']
        ]);
    }

    public function testUpdateStarAuthenticated()
    {
        $user = User::factory()->create();
        $star = Star::factory()->create();
        $updatedData = ['name' => 'Updated Name', 'firstName' => 'Updated First Name'];

        $response = $this->actingAs($user)->postJson("/api/stars/{$star->id}", $updatedData);
        $response->assertStatus(200);
        $this->assertDatabaseHas('stars', [
            'id' => $star->id,
            'name' => $updatedData['name'],
            'firstName' => $updatedData['firstName']
        ]);
    }

    public function testDeleteStarAuthenticated()
    {
        $user = User::factory()->create();
        $star = Star::factory()->create();

        $response = $this->actingAs($user)->deleteJson("/api/stars/{$star->id}");

        $response->assertStatus(200);
        $this->assertDatabaseMissing('stars', ['id' => $star->id]);
    }

    public function testShouldIncludeImageUrl()
    {
        Storage::fake('public');
        $fileName = 'testImage.jpg';
        $filePath = 'images/' . $fileName;
        Storage::disk('public')->put($filePath, 'Contents of image');

        $star = Star::create([
            'name' => 'Test Star',
            'firstName' => 'Test',
            'image' => $filePath, // Assurez-vous que cela correspond à ce que vous attendez dans votre accessor
            'description' => 'A star'
        ]);

        $response = $this->getJson('/api/stars');
        $response->assertStatus(200)
            ->assertJsonPath('data.0.imageUrl', Storage::url($filePath));
    }


    public function testShouldReturnDirectImageUrlIfItIsAlreadyAUrl()
    {
        $star = Star::create([
            'name' => 'Test Star',
            'firstName' => 'Test',
            'image' => 'http://example.com/image.jpg',
            'description' => 'A star'
        ]);

        $response = $this->getJson('/api/stars');

        $response->assertStatus(200)
            ->assertJsonPath('data.0.imageUrl', 'http://example.com/image.jpg');
    }
}
